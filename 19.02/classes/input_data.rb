class InputData
  class << self
    # Эта конструкция(class << self) позволяет вызывать методы, которые находятся внутри нее,
    # не через экземпляр класса, как прямиком из класса. Например, InputData.vms или InputData.add_data(args).
    attr_reader :vms, :additional_hdds, :price_list

    def add_data(vms, hdds, price_list)
      @vms = vms
      @additional_hdds = hdds
      @price_list = price_list
      create_vms # Сразу тут вызываем метод, чтобы он преобразовал наши данные в объекты(экземпляры) классов VirtualMachine и Hdd, и записал их в массив @vms.
    end

    def create_vms
      @vms.map! do |vm|
        new_vm = VirtualMachine.new(vm) # Тут создаем объект класса VirtualMachine
        # далее...
        new_vm_additional_hdds = @additional_hdds.select do |hdd| # ищем в дополнительных жестких дисках те, которые относятся к нашей ВМ и сохраняем их в отдельный массив new_vm_additional_hdds
          hdd[0] == vm[0]
        end

        new_vm_additional_hdds.each do |additional_hdd|
          new_vm.add_hdd(Hdd.new(additional_hdd)) # Сразу преобразовываем данные из массива в объекты класса Hdd и добавляем их в нашу новую ВМ
        end # Если не очень понятно, то посмотри как работает класс VirtualMachine

        new_vm # в методе map! обязательно возвращаем в конце то, что нужно записать массив @vms вместо vm
      end
    end
  end
end
