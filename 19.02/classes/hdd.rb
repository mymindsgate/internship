class Hdd # тут все просто)
  attr_reader :vm_id, :type, :capacity

  def initialize(data)
    @vm_id = data[0].to_i
    @type = data[1]
    @capacity = data[2].to_i
  end
end
