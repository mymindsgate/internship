class VirtualMachine
  attr_reader :id, :cpu, :ram,
              :hdd_type, :hdd_capacity,
              :additional_hdds

  def initialize(data) # Тут все по идее понятно, но если надо, то могу рассказать подробнее.
    @id = data[0].to_i
    @cpu = data[1].to_i
    @ram = data[2].to_i
    @hdd_type = data[3]
    @hdd_capacity = data[4].to_i
    @additional_hdds = []
  end

  def add_hdd(hdd) # Тот самый метод, благодаря которому мы можем добавить в виртуальную машину дополнительные диски.
    @additional_hdds << hdd
  end
end
