require 'csv'
# Вся эта идея классами помогает очень удобно и структурированно хранить информацию.
# То есть мы получим большой массив массивов, а преобразуем это в объекты, которые можно научить любому поведению. Это очень упростит работу.

# с помощью метода require_relative я подключаю нужные классы в этот файл (app.rb)
# require_relative в данном случае заходит в папку classes и оттуда берет нужные файлы (писать расширение .rb не нужно).

require_relative 'classes/virtual_machine' # В классе VirtualMachine мы будем обучать нашу программу работать с виртуальными машинами..
# Научим создавать объекты этого класса и получать нужную из них информацию. Будь то количество ядер или объем жесткого диска.

require_relative 'classes/hdd' # Класс Hdd будет создавать объекты, которые ведут себя как дополнительные жесткие диски.

require_relative 'classes/report' # Класс Report будет получать все виртуальные машины, прайс-лист и отвечать на вопросы, полученные в домашнем задании.

require_relative 'classes/input_data' # Класс InputData будет получать данные из CSV файлов и преобразовывать их в объекты классов Hdd и VirtualMachine.

vms = CSV.read("#{File.dirname(__FILE__)}/input/vms.csv")
additional_hdds =  CSV.read("#{File.dirname(__FILE__)}/input/volumes.csv")
price_list = CSV.read("#{File.dirname(__FILE__)}/input/prices.csv").to_h

# Повторюсь, класс InputData хранит в себе всю полученную информацию из файлов CSV.
# И именно там происходит создание вирутальных машин и жестких дисков.
InputData.add_data(vms, additional_hdds, price_list)


# Класс Report хранит в себе методы, которые будут выводить все нужные результаты.
report = Report.new(InputData.vms, InputData.price_list)

# Снизу if создан, чтобы программа работала при запуске контейнера.
if ENV['n']
  n = ENV['n'].to_i
else
  n = 3
end

report.most_expensive_vms(n)
report.cheapest_vms(n)
report.most_capacious_vms(n, 'CPU')
report.most_additonal_hdds(n)
report.biggest_extra_space(n)
